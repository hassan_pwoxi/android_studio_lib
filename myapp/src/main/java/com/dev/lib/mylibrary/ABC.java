package com.dev.lib.mylibrary;

/**
 * Created by Hassan on 6/10/2015.
 */
public class ABC {
    private int a = 0;

    public ABC(int x) {
        a = x;
    }

    private int xyz() {
        return a;
    }
}
